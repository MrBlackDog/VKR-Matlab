%%
[AccVal_1,GyrVal_1,MagVal_1,QuatVal_1,EulerVal_1,INSobs_1] = GNSS_Logger_INS(0);
%% ������ ����
Ref_TimeStamp =  [0.0 0.10 0.20 0.245 0.30 0.36 0.4 0.41 0.5];
Ref_Step_Value = [0.5 0.58 0.89 1.000 0.77 0.66 0.7 0.71 0.5];

Ref_interp_TimeStamp =  0:0.001:0.5;
Ref_interp_Step_Value = interp1(Ref_TimeStamp,Ref_Step_Value,Ref_interp_TimeStamp,'spline');

%% Compeletnary filter
q_FC_in = [1;0;0;0];
I = eye(4);
%% ����� �������
Norm_Acc_Val = Acc_Norm(AccVal);
%% ���������� �������
svertka =zeros(length(Norm_Acc_Val),1);
New_Acc_Norm = zeros(length(Norm_Acc_Val)+length(Ref_Step_Value),1);
New_Acc_Norm(length(Ref_Step_Value)+1:end) = Norm_Acc_Val;
for i=length(Ref_Step_Value)+1:length(New_Acc_Norm)
    for j=1:length(Ref_Step_Value)
        svertka(i-length(Ref_Step_Value)) = svertka(i-length(Ref_Step_Value)) +...
            New_Acc_Norm(i-length(Ref_Step_Value)+j)*Ref_Step_Value(j);
    end
end

%% Calibration based on Quasi-Static state
Acc0x = 0;
Acc0y = 0;
Acc0z = 0;
Gyr0x = 0;
Gyr0y = 0;
Gyr0z = 0;
Gyr_bias_x = 0;
Gyr_bias_y = 0;
Gyr_bias_z = 0;

CalibrationFinished = 0;
gamma = 20;
sigma_Acc = 0.06;
sigma_Gyr = 0.01;
N=30;

%%
Quasi_State_Count = 1;
for i=N+1:size(AccVal,1)-N
        Quasi_State_val(i) = 1/N * sum (norm(AccVal(i,2:4)'-mean(AccVal(i-N:i,2:4))')^2/sigma_Acc^2 + norm(GyrVal(i,2:4))^2/sigma_Gyr^2);
        if(Quasi_State_val(i) > gamma)
            Quasi_State_flag(i) = 1;
            if(~isempty(Acc0x) && ~isempty(Acc0y) && ~isempty(Acc0z) && ~CalibrationFinished)
                Acc0x(1:Quasi_State_Count) = [];
                Acc0y(1:Quasi_State_Count) = [];
                Acc0z(1:Quasi_State_Count) = [];
%                 Gyr0x(1:Quasi_State_Count) = [];
%                 Gyr0y(1:Quasi_State_Count) = [];
%                 Gyr0z(1:Quasi_State_Count) = [];
                Quasi_State_Count = 1;
            end
        else 
            Quasi_State_flag(i) = 0;
            if(Quasi_State_flag(i-1) ==0) 
                if(~CalibrationFinished)
                    Quasi_State_Count = Quasi_State_Count + 1;
                    Quasi_State_Time(Quasi_State_Count) = AccVal(i,1);            
                    Acc0x(Quasi_State_Count) = AccVal(i,2);
                    Acc0y(Quasi_State_Count) = AccVal(i,3);
                    Acc0z(Quasi_State_Count) = AccVal(i,4);
%                     Gyr0x(
                end
            end
            if(Quasi_State_Count == 1000 && CalibrationFinished ==0)
               CalibrationFinished = 1;
               CalibrationFinishedTime = AccVal(i,1);
               mAcc0x = mean(Acc0x);
               mAcc0y = mean(Acc0y);
               mAcc0z = mean(Acc0z);
            end
        end
end

%% delta Norm Acc
   delAcc = del_Acc_Filter(AccVal,[0, 0, 9.8]);
   
%% smoothed delta Norm Acc
N=20; %window size
for mm=N+1:length(delAcc)-N
    AccSm(mm-N)= 1/(2*N+1)*sum(delAcc(mm-N:mm+N)); %moving average 
end
AccSmTime= AccVal(1:length(AccVal)-2*N,1)';
%% ���������� �������
Tpeak = 7;
conv_step_count=0;
conv_step_detected=0;
Acc_conv = conv(AccSm,Ref_Step_Value);
for i=1:length(Acc_conv)
    if(Acc_conv(i)>Tpeak )
        if(Acc_conv(i) > Acc_conv(i-1))
            if(conv_step_detected == 0)
                conv_step_detected = 1;
                conv_step_count = conv_step_count + 1; 
            end
            conv_step_val(conv_step_count) = Acc_conv(i);
            conv_step_time(conv_step_count) = AccSmTime(i);
        else
            conv_step_detected=0;
        end
    end
    
end
%% Step Detection
THpeak = 1.5;
THvalley = -0.4;
THpeak_valley = 0.2*1e9;
THvalley_peak = 0.2*1e9;
%%
flag_1_count = 0;
flag_2_count = 0;
flag_3_count = 0;
stepscount = 0;
flag_1_detected = 0;
flag_2_detected = 0;
flag_3_detected = 0;
for mmm =1:length(AccSm)
    if(AccSm(mmm)>THpeak )
        if(AccSm(mmm) > AccSm(mmm-1))
            if(flag_1_detected == 0)
                flag_1_detected = 1;
                flag_1_count = flag_1_count + 1; 
%         elseif
            end
            flag_1_value(flag_1_count) = AccSm(mmm-1);
            flag_1_time(flag_1_count) = AccSmTime(mmm-1);
        end   
    end
    if(flag_1_detected)
        if((AccSm(mmm) < THvalley) && ((AccSmTime(mmm) - flag_1_time(flag_1_count)) > THpeak_valley))
            if(AccSm(mmm) < AccSm(mmm-1))
                if(flag_2_detected == 0)
                flag_2_count = flag_2_count + 1;
%                   flag_1_detected = 0;
                flag_2_detected = 1;
                end
                flag_2_value(flag_2_count) = AccSm(mmm-1);
                flag_2_time(flag_2_count) = AccSmTime(mmm-1);
            end
        end
    end
    if(flag_2_detected &&... 
       AccSm(mmm)>THpeak && ...
       AccSmTime(mmm) - flag_2_time(flag_2_count) > THvalley_peak)
       stepscount = stepscount +1;
       steptime(stepscount) = AccSmTime(mmm-1);
       stepVal(stepscount) = AccSm(mmm-1);
       flag_2_detected = 0;
       flag_1_detected = 0;
    end
end