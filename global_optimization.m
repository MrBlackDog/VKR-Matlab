step= 1:1:6;
k = 1:1:10;
%% exp 1 2;4
kNN_all_1 = [[3.30	2.25	2.93	3.35	3.60	3.92	4.09	4.11	4.15	4.22]
[3.50	2.34	3.16	3.44	3.81	3.84	4.02	4.16	4.26	4.23];
[3.68	2.95	3.44	3.45	3.67	4.09	4.32	4.20	4.27	NaN];
[2.11	3.23	4.57	4.30	NaN	NaN	NaN	NaN	NaN	NaN];
[3.00	3.43	4.33	4.10	NaN	NaN	NaN	NaN	NaN	NaN];
[2.97	2.90	4.03	3.88	NaN	NaN	NaN	NaN	NaN	NaN]];

kNN_one_1 = [[2.94	2.63	3.61	3.76	4.03	4.13	4.10	4.07	4.29	4.11];
[3.89	3.89	3.77	3.5	4.05	4.08	3.82	4.04	4.33	4.22];
[2.56	2.66	2.56	4.58	3.85	3.33	3.97	4.50	4.32	NaN];
[3.70	3.49	3.45	4.31	NaN	NaN	NaN	NaN	NaN	NaN];
[4.57	1.83	3.36	4.29	NaN	NaN	NaN	NaN	NaN	NaN];
[4.67	1.31	3.14	3.96	NaN	NaN	NaN	NaN	NaN	NaN]];

WkNN_all_1 = [[3.3	2.23	2.00	1.86	1.76	1.66	1.53	1.46	1.43	1.41];
[3.53	2.84	2.07	1.74	1.51	1.43	1.33	1.23	1.16	1.15;]
[3.73	2.53	1.83	1.29	1.43	1.48	1.41	1.44	1.44	NaN];
[2.11	0.93	1.38	1.65	NaN	NaN	NaN	NaN	NaN	NaN];
[3.00	1.60	1.51	1.49	NaN	NaN	NaN	NaN	NaN	NaN];
[2.97	2.04	1.17	0.92	NaN	NaN	NaN	NaN	NaN	NaN]];

WkNN_one_1 = [[2.92	2.67	2.25	2.13	1.93	1.80	1.62	1.51	1.45	1.4];
[3.89	2.11	1.96	1.68	1.46	1.25	1.05	0.99	1.07	1.12];
[2.59	1.33	1.02	1.19	1.20	1.04	0.84	0.95	1.19	NaN];
[3.70	2.50	1.84	2.03	NaN	NaN	NaN	NaN	NaN	NaN];
[4.57	2.06	1.39	1.80	NaN	NaN	NaN	NaN	NaN	NaN];
[4.67	1.44	0.80	1.04	NaN	NaN	NaN	NaN	NaN	NaN]];

kNN_MLC_all_1 =[[3.29	2.08	2.78	3.15	3.43	3.72	3.88	3.93	4.05	4.25];
[3.55	2.39	3.17	3.54	3.74	3.99	4.14	4.16	4.32	4.32];
[2.94	2.73	3.28	3.71	4.08	4.20	4.09	4.13	4.17	NaN];
[2.93	3.36	4.14	4.32	NaN	NaN	NaN	NaN	NaN	NaN];
[3.05	3.81	4.10	4.14	NaN	NaN	NaN	NaN	NaN	NaN];
[3.82	3.55	3.91	3.46	NaN	NaN	NaN	NaN	NaN	NaN]];

kNN_MLC_one_1 = [[3.27	2.57	3.68	3.85	3.96	4.19	4.12	4.42	4.26	4.51];
[3.88	2.70	3.40	3.92	4.32	4.06	3.96	4.31	4.24	4.13];
[2.77	2.68	3.20	3.70	4.51	4.00	4.21	4.11	3.92	NaN];
[3.46	3.94	3.92	3.81	NaN	NaN	NaN	NaN	NaN	NaN];
[4.57	3.78	3.62	3.20	NaN	NaN	NaN	NaN	NaN	NaN];
[3.24	4.34	3.72	2.82	NaN	NaN	NaN	NaN	NaN	NaN]];
%% exp 2 3;5.6
kNN_all_2 = [[3.49	4.27	5.00	5.24	5.44	5.66	5.80	5.82	5.98	5.99]
[4.52	4.62	5.08	5.42	5.55	5.65	5.76	5.87	5.91	5.93];
[3.72	4.96	5.05	5.49	5.55	5.77	5.87	5.87	5.79	NaN];
[5.22	4.91	5.72	5.69	NaN	NaN	NaN	NaN	NaN	NaN];
[5.23	4.95	5.20	5.57	NaN	NaN	NaN	NaN	NaN	NaN];
[5.60	4.25	4.91	5.12	NaN	NaN	NaN	NaN	NaN	NaN]];

kNN_one_2 = [[ 4.27	4.06	4.74	5.58	5.38	5.70	5.67	5.80	6.07	5.89];
[5.21	5.03	4.69	5.45	5.87	5.55	5.41	5.96	6.10	5.61];
[2.30	5.43	4.88	5.60	5.19	5.45	5.76	6.23	6.01	NaN];
[6.26	3.94	5.87	5.57	NaN	NaN	NaN	NaN	NaN	NaN];
[6.35	3.24	5.02	5.92	NaN	NaN	NaN	NaN	NaN	NaN];
[6.36	2.20	4.77	5.60	NaN	NaN	NaN	NaN	NaN	NaN]];

WkNN_all_2 = [[3.49	2.94	2.82	2.70	2.64	2.60	2.57	2.53	2.51	2.50];
[4.52	3.68	3.43	3.31	3.07	2.95	2.85	2.77	2.70	2.66];
[3.72	3.09	2.89	2.81	2.76	2.74	2.69	2.57	2.48	NaN];
[5.22	4.46	4.43	4.37	NaN	NaN	NaN	NaN	NaN	NaN];
[5.23	4.42	4.03	3.86	NaN	NaN	NaN	NaN	NaN	NaN];
[5.60	4.01	3.39	3.01	NaN	NaN	NaN	NaN	NaN	NaN]];

WkNN_one_2 = [[4.28	3.60	3.45	3.51	3.52	3.56	3.54	3.47	3.45	3.43];
[5.21	3.81	3.16	3.14	3.07	2.95	2.78	2.80	2.83	2.72];
[2.29	2.41	2.07	2.16	2.05	2.00	1.91	2.00	2.04	NaN];
[6.27	4.16	4.21	4.15	NaN	NaN	NaN	NaN	NaN	NaN];
[6.37	4.63	4.16	4.19	NaN	NaN	NaN	NaN	NaN	NaN];
[6.37	3.96	3.52	3.50	NaN	NaN	NaN	NaN	NaN	NaN]];


kNN_MLC_all_2 = [[3.25	4.48	4.79	5.32	5.53	5.60	5.83	5.92	6.00	6.02];
[4.41	5.06	5.13	5.42	5.59	5.74	5.73	5.87	5.96	5.91];
[3.72	4.89	5.19	5.36	5.56	5.63	5.51	5.86	6.15	NaN];
[5.38	4.83	5.65	5.73	NaN	NaN	NaN	NaN	NaN	NaN];
[6.25	4.74	5.08	5.26	NaN	NaN	NaN	NaN	NaN	NaN];
[5.83	4.73	4.12	5.32	NaN	NaN	NaN	NaN	NaN	NaN]];

kNN_MLC_one_2 = [[2.48	4.04	4.23	4.89	5.61	5.44	6.01	6.11	6.08	6.15];
[2.64	6.33	4.94	5.25	5.34	5.99	5.41	5.96	6.09	5.79];
[2.24	5.34	5.71	5.01	5.64	5.54	5.36	6.12	6.12	NaN];
[4.36	5.47	5.88	5.56	NaN	NaN	NaN	NaN	NaN	NaN];
[6.28	4.45	4.97	5.47	NaN	NaN	NaN	NaN	NaN	NaN];
[5.83	4.73	4.12	5.32	NaN	NaN	NaN	NaN	NaN	NaN]];

%% exp 3 6;3.5
kNN_all_3 = [[3.43	4.71	5.68	6.03	6.09	6.34	6.47	6.49	6.68	6.62];
[4.14	4.54	5.43	6.01	6.46	6.57	6.52	6.71	6.70	6.67];
[3.18	5.76	5.85	6.35	6.42	6.75	6.49	6.52	6.41	NaN];
[3.36	6.25	6.43	6.66	NaN	NaN	NaN	NaN	NaN	NaN];
[5.62	6.11	5.99	6.10	NaN	NaN	NaN	NaN	NaN	NaN];
[6.42	5.77	6.05	5.34	NaN	NaN	NaN	NaN	NaN	NaN]];

kNN_one_3 = [[3.81	4.38	5.60	6.18	5.86	6.42	6.45	6.55	6.61	6.66];
[4.30	5.16	5.27	6.10	6.40	6.65	6.43	6.73	6.79	6.64];
[3.70	5.81	5.66	6.21	6.37	6.72	6.31	6.83	6.40	NaN];
[3.76	5.74	6.31	6.92	NaN	NaN	NaN	NaN	NaN	NaN];
[5.23	5.62	6.36	6.21	NaN	NaN	NaN	NaN	NaN	NaN];
[6.54	4.99	6.37	5.50	NaN	NaN	NaN	NaN	NaN	NaN]];

WkNN_all_3 = [[3.43	3.22	3.26	3.24	3.18	3.15	3.14	3.13	3.15	3.14];
[4.14	3.27	3.02	3.03	3.12	3.22	3.24	3.29	3.33	3.35];
[3.18	3.17	3.14	3.33	3.47	3.63	3.56	3.44	3.33	NaN];
[3.36	3.54	3.85	4.03	NaN	NaN	NaN	NaN	NaN	NaN];
[5.62	5.07	4.67	4.41	NaN	NaN	NaN	NaN	NaN	NaN];
[6.42	5.19	4.58	3.90	NaN	NaN	NaN	NaN	NaN	NaN]];

WkNN_one_3 = [[3.18	2.90	2.95	3.04	2.99	3.01	3.00	3.04	3.04	3.05];
[4.30	3.64	3.33	3.24	3.26	3.39	3.37	3.42	3.45	3.43];
[3.61	3.50	3.50	3.38	3.57	3.57	3.40	3.50	3.40	NaN];
[3.76	3.54	3.80	4.09	NaN	NaN	NaN	NaN	NaN	NaN];
[5.23	4.33	4.41	4.29	NaN	NaN	NaN	NaN	NaN	NaN];
[6.54	4.74	4.64	4.07	NaN	NaN	NaN	NaN	NaN	NaN]];

kNN_MLC_all_3 = [[3.05	4.59	5.60	5.66	6.22	6.29	6.49	6.74	6.63	6.65];
[3.49	6.17	5.80	6.33	6.36	6.59	6.54	6.62	6.63	6.64];
[3.54	5.88	6.25	6.05	6.44	6.38	6.51	6.69	6.57	NaN];
[4.30	6.70	6.00	6.54	NaN	NaN	NaN	NaN	NaN	NaN];
[5.71	6.31	5.70	6.18	NaN	NaN	NaN	NaN	NaN	NaN];
[5.34	6.23	5.81	5.81	NaN	NaN	NaN	NaN	NaN	NaN]];


kNN_MLC_one_3 = [[2.07	4.13	5.46	5.73	6.28	6.08	6.37	6.65	6.51	6.68];
[2.47	5.59	5.43	6.11	6.24	6.58	6.55	6.66	6.69	6.77];
[2.48	5.09	6.26	6.33	6.16	6.43	6.50	6.70	6.79	NaN];
[2.73	6.38	6.13	6.92	NaN	NaN	NaN	NaN	NaN	NaN];
[4.48	6.24	5.86	6.38	NaN	NaN	NaN	NaN	NaN	NaN];
[5.50	4.93	5.84	6.55	NaN	NaN	NaN	NaN	NaN	NaN]];

%% opt
kNN_all_mean = (kNN_all_1 + kNN_all_2 + kNN_all_3)./3;
kNN_one_mean = (kNN_one_1 + kNN_one_2 + kNN_one_3)./3;
WkNN_all_mean = (WkNN_all_1 + WkNN_all_2 + WkNN_all_3)./3;
WkNN_one_mean = (WkNN_one_1 + WkNN_one_2 + WkNN_one_3)./3;
kNN_MLC_all_mean = (kNN_MLC_all_1 + kNN_MLC_all_2 + kNN_MLC_all_3)./3;
kNN_MLC_one_mean = (kNN_MLC_one_1 + kNN_MLC_one_2 + kNN_MLC_one_3)./3;
%% surfs
%% kNN_all_mean
figure()
surf(k,step, kNN_all_mean)
xlabel('число соседей,k')
ylabel('шаг сетки,м')
c = colorbar;
c.Label.String = '\sigma_{xy},м';
xticks(1:1:10);
% xticklabels({'k = 1','k = 2','k = 3','k = 4','k = 5','k = 6','k = 7','k = 8','k = 9','k = 10'});
yticks([1 2 3 4 5 6]);
yticklabels(1:1:6);
%% kNN_one_mean
figure()
surf(k,step, kNN_one_mean)
xlabel('число соседей,k')
ylabel('шаг сетки,м')
c = colorbar;
c.Label.String = '\sigma_{xy},м';
xticks(1:1:10);
% xticklabels({'k = 1','k = 2','k = 3','k = 4','k = 5','k = 6','k = 7','k = 8','k = 9','k = 10'});
yticks([1 2 3 4 5 6]);
yticklabels(1:1:6);
%% WkNN_all_mean
figure()
surf(k,step, WkNN_all_mean)
xlabel('число соседей,k')
ylabel('шаг сетки,м')
c = colorbar;
c.Label.String = '\sigma_{xy},м';
xticks(1:1:10);
% xticklabels({'k = 1','k = 2','k = 3','k = 4','k = 5','k = 6','k = 7','k = 8','k = 9','k = 10'});
yticks([1 2 3 4 5 6]);
yticklabels(1:1:6);
%% WkNN_one_mean
figure()
surf(k,step, WkNN_one_mean)
xlabel('число соседей,k')
ylabel('шаг сетки,м')
c = colorbar;
c.Label.String = '\sigma_{xy},м';
xticks(1:1:10);
% xticklabels({'k = 1','k = 2','k = 3','k = 4','k = 5','k = 6','k = 7','k = 8','k = 9','k = 10'});
yticks([1 2 3 4 5 6]);
yticklabels(1:1:6);
%% kNN_MLC_all_mean
figure()
surf(k,step, kNN_MLC_all_mean)
xlabel('число соседей,k')
ylabel('шаг сетки,м')
c = colorbar;
c.Label.String = '\sigma_{xy},м';
xticks(1:1:10);
% xticklabels({'k = 1','k = 2','k = 3','k = 4','k = 5','k = 6','k = 7','k = 8','k = 9','k = 10'});
yticks([1 2 3 4 5 6]);
yticklabels(1:1:6);
%% kNN_MLC_one_mean
figure()
surf(k,step, kNN_MLC_one_mean)
xlabel('число соседей,k')
ylabel('шаг сетки,м')
c = colorbar;
c.Label.String = '\sigma_{xy},м';
xticks(1:1:10);
% xticklabels({'k = 1','k = 2','k = 3','k = 4','k = 5','k = 6','k = 7','k = 8','k = 9','k = 10'});
yticks([1 2 3 4 5 6]);
yticklabels(1:1:6);