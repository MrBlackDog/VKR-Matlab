function [p] = kNN_method(s,wifi_database,k)

d = zeros(size(wifi_database,2),size(s.meas,2));
count = zeros(size(wifi_database,2),1);
for n=1:size(wifi_database,2)
    for l=1:size(s.meas,2)
        TF = matches(extractfield(wifi_database(n).meas,'BSSID'),s.meas(l).BSSID,'IgnoreCase',1);
        if(TF(TF==1))
            d(n,l) = (s.meas(l).RSSI - wifi_database(n).meas(TF).meanRSSI)^2;
            count(n) = count(n) + 1;
        end
    end
    try          
        sim(n) = sqrt(sum(d(n,:)))^-1;
    catch
        sim(n) = NaN;
    end
end
[sorted_sim ind]= sort(sim);
TF = ~isnan(sorted_sim);
non_nan_ind = ind(TF);
sorted_sim_non_nan = sorted_sim(TF);
p(1) = sum(str2double(wifi_database(non_nan_ind(end-k+1)).coordinate.x))/k;
p(2) = sum(str2double(wifi_database(non_nan_ind(end-k+1)).coordinate.y))/k;
end

