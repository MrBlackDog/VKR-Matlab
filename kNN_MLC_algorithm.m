function [p] = kNN_MLC_algorithm(s,wifi_database,k)

for i=1:size(wifi_database,2)
    for m=1:size(s.meas,2)
        TF = matches(extractfield(wifi_database(i).meas,'BSSID'),s.meas(m).BSSID,'IgnoreCase',1);
        if(TF(TF==1))
            P(i,m) = 1/(wifi_database(i).meas(TF).sigmaRSSI*sqrt(2*pi))...
                * exp(-1*0.5*( (s.meas(m).RSSI-wifi_database(i).meas(TF).meanRSSI)/wifi_database(i).meas(TF).sigmaRSSI)^2);
        else
            P(i,m) = 0;
        end
    end
end
[sorted_sim ind]= sort(sum(P,2));
TF = ~isnan(sorted_sim);
non_nan_ind = ind(TF);
p(1) = sum(str2double(wifi_database(non_nan_ind(end-k+1)).coordinate.x))/k;
p(2) = sum(str2double(wifi_database(non_nan_ind(end-k+1)).coordinate.y))/k;
end