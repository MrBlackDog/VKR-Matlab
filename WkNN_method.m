function [p] = WkNN_method(s,wifi_database,k)

d = zeros(size(wifi_database,2),size(s.meas,2));
count = zeros(size(wifi_database,2),1);
for n=1:size(wifi_database,2)
    for l=1:size(s.meas,2)
        TF = matches(extractfield(wifi_database(n).meas,'BSSID'),s.meas(l).BSSID,'IgnoreCase',1);
        if(TF(TF==1))
            d(n,l) = (s.meas(l).RSSI - wifi_database(n).meas(TF).meanRSSI)^2;
            count(n) = count(n) + 1;
        else
            d(n,l) = NaN;
        end
    end
    
    try
        TF = ~isnan(d(n,:));
        if(~isempty(d(n,TF)))
            sim(n) = sqrt(sum(d(n,TF)))^-1;
        else
            sim(n) = NaN;
        end
    catch
        sim(n) = NaN;
    end
end

[sorted_sim ind]= sort(sim);
TF = ~isnan(sorted_sim) & ~isinf(sorted_sim);
non_nan_ind = ind(TF);
sorted_sim_non_nan = sorted_sim(TF);
for j=0:k-1
    wknn(1,j+1) = sorted_sim_non_nan(end-j)*str2double(wifi_database(non_nan_ind(end-j)).coordinate.x);
    wknn(2,j+1) = sorted_sim_non_nan(end-j)*str2double(wifi_database(non_nan_ind(end-j)).coordinate.y); 
end
p(1) = sum(wknn(1,:))/sum(sorted_sim_non_nan(end-k+1:end));
p(2) = sum(wknn(2,:))/sum(sorted_sim_non_nan(end-k+1:end));
end