%% �� ���� �������
% ������,�������� �������� ������� ��
% ����� ��� ������ ��������� ������ � ��������, ��� ����� ���� ��������
%% ������������� �������� ����������� � ��������� ���������
SatPos= [ 0 0 100 100;0 100 0 100];
x_true = 0:1:99;
y_true = 50*ones(1,size(x_true,2));
%% ������������� ��������� �� �������� �����
for i=1:size(x_true,2)
    R_true(i,1) = sqrt((SatPos(1,1)-x_true(i))^2 + (SatPos(2,1)-y_true(i))^2);
    R_true(i,2) = sqrt((SatPos(1,2)-x_true(i))^2 + (SatPos(2,2)-y_true(i))^2);
    R_true(i,3) = sqrt((SatPos(1,3)-x_true(i))^2 + (SatPos(2,3)-y_true(i))^2);
    R_true(i,4) = sqrt((SatPos(1,4)-x_true(i))^2 + (SatPos(2,4)-y_true(i))^2);
%���� ��������� � �������� (R_true -> � P_true)
%������� ������ https://ru.wikipedia.org/wiki/����������_������_������������_�������
    P_true(i,1) = 0 - 10*2*log10(R_true(i,1));
    P_true(i,2) = 0 - 10*2*log10(R_true(i,2));
    P_true(i,3) = 0 - 10*2*log10(R_true(i,3));
    P_true(i,4) = 0 - 10*2*log10(R_true(i,4));
end
%% ������������� ���������
%� ������ ��������� � ���� �������� ������� ��� ����������,��������� ������
%�����.
sigma_n = 5;
for i=1:size(x_true,2)
    R_measured(i,1) = R_true(i,1) + normrnd(0,sigma_n);
    R_measured(i,2) = R_true(i,2) + normrnd(0,sigma_n);
    R_measured(i,3) = R_true(i,3) + normrnd(0,sigma_n);
    R_measured(i,4) = R_true(i,4) + normrnd(0,sigma_n);
    P_measured(i,1) = 0 - 10*2*log10(R_measured(i,1));
    P_measured(i,2) = 0 - 10*2*log10(R_measured(i,2));
    P_measured(i,3) = 0 - 10*2*log10(R_measured(i,3));
    P_measured(i,4) = 0 - 10*2*log10(R_measured(i,4));
end
%%
% ����� ���� ������ ����� �� ��������� � ��������� ��� ��� ���������
% ��������
% ����� � ��� ������� 100 �� 100
% ����� ������� � ����� � 2
size_x = 100;
size_y = 100;
step = 2;
x_grid = 1;
y_grid = 1;
for m=1:1:size_x/2 - 1
    y_grid = 1;
    for n=1:1:size_y/2 - 1
        R_database(n,m,1) = sqrt((SatPos(1,1)-x_grid)^2 + (SatPos(2,1)-y_grid)^2);
        R_database(n,m,2) = sqrt((SatPos(1,2)-x_grid)^2 + (SatPos(2,2)-y_grid)^2);
        R_database(n,m,3) = sqrt((SatPos(1,3)-x_grid)^2 + (SatPos(2,3)-y_grid)^2);
        R_database(n,m,4) = sqrt((SatPos(1,4)-x_grid)^2 + (SatPos(2,4)-y_grid)^2);
        P_database(n,m,1).val = 0 - 10*2*log10(R_database(n,m,1));
        P_database(n,m,2).val = 0 - 10*2*log10(R_database(n,m,2));
        P_database(n,m,3).val = 0 - 10*2*log10(R_database(n,m,3));
        P_database(n,m,4).val = 0 - 10*2*log10(R_database(n,m,4));
        y_grid = y_grid + step;
    end
    x_grid = x_grid + step;
end

%% ���������� ���������
% � ������ ������, ����� ��� �������� ���������, �� ���������� �� ����
% �������� ������ � ������� ����� ������� ����� ���������� ��������� ��
% �������� ����� � �������� �������� ��������� � ���� �����

% ���������, �� �������� ���������� ����������, ��� �� ��������� ��������
% ����� � �����, � "���������� ������ ����������� ���� �������"
max_count = 0;  
for x=1:1:size_x/2 - 1
    for y=1:1:size_y/2 - 1
        for k=1:1:size(SatPos,2)
            curr_min(k) = sqrt(P_measured(x,k)^2-P_true(x,k)^2);
            k_min(k)=k;
        end
    end
end
c=0;
max_count=curr_min(1,1);
for k=1:1:size(SatPos,2)
   for x=1:1:size_x/2 - 1
    for y=1:1:size_y/2 - 1
            if (max_count<curr_min(x,y))
                max_count=curr_min(x,y);
                x_min=x;
                y_min=y;
                k_n=k_min(k);
                c=c+1;
            end
    end
   end   
end
disp(max_count);
disp(c);
disp(x_min);
disp(y_min);
disp(k_n);
%% rigth version
max_count = 0;  
diff = [];
for x=1:1:size_x/2 - 1
    for y=1:1:size_y/2 - 1
        for k=1:1:size(SatPos,2)
            P_database(x,y,k).diff = abs(P_database(x,y,k).val - P_measured(1,k));
            diff(x,y,k) = abs(P_database(x,y,k).val - P_measured(1,k));
            k_min(k)=k;
        end
    end
end

%% find min of matrix
minrow = [];
minrowind = [];
for k=1:size(diff,3)
    for i=1:size(diff,1)
        [minrow(i,k), minrowind(i,k)] = min(diff(i,:,k),[],2);
%         [minval(k), mincolind(k)] = min(minrow(k));
    end
    [minval(k), mincolind(k)] = min(minrow(:,k));
end
% [min mincolind] = min(minrow);
% diff(minrowind(mincolind),mincolind,:)% shows min of matrix