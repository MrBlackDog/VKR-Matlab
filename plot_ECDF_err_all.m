function [] = plot_ECDF_err_all(X,mx,my)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
figure()
hold on
for i=1:10
    err_x(i,:) = mx - X(1,:,i);
    err_y(i,:) = my - X(2,:,i);
    [f1,x1]=ecdf(sqrt(err_x(i,:).^2 + err_y(i,:).^2));
    plot(x1,f1,'LineWidth',2)
    %      plot(ecdf(WkNN_err(1,:,i)))
end
legend('k=1','k=2','k=3','k=4','k=5','k=6','k=7','k=8','k=9','k=10')
grid on
xlabel('\sigma_{xy},м')
ylabel('F(\sigma_{xy})')
end

