function [X_kNN,X_kNN_1AP] = kNN_algorithm_solver(wifi_meas,wifi_meas_1AP,wifi_database)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
parfor i=1:size(wifi_meas,2)    
    for k=1:11
        %k=1 is the same point
        try
            X_kNN(:,i,k) = kNN_method(wifi_meas(i),wifi_database,k);
        catch
            X_kNN(:,i,k) = [NaN;NaN];
        end
        try
            X_kNN_1AP(:,i,k) = kNN_method(wifi_meas_1AP(i),wifi_database,k);
        catch
            X_kNN_1AP(:,i,k) = [NaN;NaN];
        end
    end
end
end