% for i=1:size(extractfield(wifi_smart_meas,'meas'),2)
%     if(contains(extractfield(wifi_smart_meas(i).meas,'SSID','RTS_G400')))
%         meas_mass(i) = str2num(wifi_smart_meas(i).meas(contains(extractfield(wifi_smart_meas(i).meas,'SSID'),'RTS_G400')).RSSI);
%     else
%         meas_mass(i) = 0;
%     end
% end
% 
% %%
% radio_time = 1;
% radio_timestamp = extractfield(wifi_smart_meas,'timestamp');
% 
% for ins=1:size(conv_step_time_1th,2)
%     if((radio_timestamp(radio_time)<conv_step_time_1th(ins)) &&...
%        (conv_step_time_1th(ins)<radio_timestamp(radio_time+1)))
% %         err(1,ins) = x4(1,ins) - X2(1,radio_time);
% %         err(2,ins) = x4(2,ins) - X2(2,radio_time);
% %         err2(1,radio_time) = x4(1,ins) - X2(1,radio_time);
% %         err2(2,radio_time) = x4(2,ins) - X2(2,radio_time);
%         x_est(1,ins) = x4(1,ins) - err2(1,radio_time);
%         x_est(2,ins) = x4(2,ins) - err2(2,radio_time);
%     else     
% %         err(1,ins) = x4(1,ins) - X2(1,radio_time);
% %         err(2,ins) = x4(2,ins) - X2(2,radio_time);
%         radio_time = radio_time + 1;
%         err2(1,radio_time) = x4(1,ins) - X2(1,radio_time);
%         err2(2,radio_time) = x4(2,ins) - X2(2,radio_time);
%         x_est(1,ins) = x_est(1,ins-1) - err2(1,radio_time);
%         x_est(2,ins) = x_est(2,ins-1) - err2(2,radio_time);
%     end
% end
%% timestamps of measurements
figure()
plot(extractfield(wifi_smart_meas,'timestamp'),ones(size(extractfield(wifi_smart_meas,'timestamp'),1)),'x b')
hold on
plot(conv_step_time_1th,ones(size(conv_step_time_1th,2)),'o r')
%%
radio_time = 0;
radio_timestamp = extractfield(wifi_smart_meas,'timestamp');
% err2(:,radio_time) = [0;0];
for ins=1:size(conv_step_time_1th,2)
    if(conv_step_time_1th(ins)>radio_timestamp(radio_time+1))
        radio_time = radio_time + 1;
        err2(1,radio_time) = x7(2,ins) - X(1,radio_time);
        err2(2,radio_time) = x7(1,ins) - X(2,radio_time);
    end
    x8(1,ins)= x7(2,ins) - err2(1,radio_time);
    x8(2,ins)= x7(1,ins) - err2(2,radio_time);
end

%%
figure()
h = plot(0,0,'b')
hold on;
axis([-10 10 0 70]);
for ins=1:size(conv_step_time_1th,2)
    set(h,'XData',x4(1,1:ins),'YData',x4(2,1:ins),'LineWidth',1)
    pause(1);
end
