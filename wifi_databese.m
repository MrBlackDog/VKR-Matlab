%wifi_database_creator
[wifi_database] = wifi_log_reader_v1();
[wifi_database] = wifi_log_parser(wifi_database);
%%
[wifi_smart_meas] = wifi_smart_log_reader();
[uwb] = uwb_log_reader();
%%
[wifi_meas] = error_meas_removal(wifi_smart_meas);
%% ref 2d
j=0;
pos = zeros(3,length(uwb));
for i=1:size(uwb,2)
    if(~isempty(extractfield(uwb(i),'tag_pos')))
        j=j+1;
        time(j) = str2num(uwb(i).timestamp{1,1});
        pos(:,j) = extractfield(uwb(i),'tag_pos')';
%     catch ME
%         j=j-1;
    end
end
plot(pos(1,:),pos(2,:))
%% ref 1d
j=0;
pos = zeros(2,length(uwb));
for i=1:size(uwb,2)
    if(~isempty(extractfield(uwb(i).data,'dist')))
        j=j+1;
        time(j) = str2num(uwb(i).timestamp{1,1});
        pos(1,j) = 0;
        pos(2,j) = extractfield(uwb(i).data,'dist');
%     catch ME
%         j=j-1;
    end
end
%%
SatPos = [uwb(1).data(1).anch_pos;
          uwb(1).data(2).anch_pos;
          uwb(1).data(3).anch_pos;
          uwb(1).data(4).anch_pos];
SatPos = SatPos';
for i=1:size(uwb,2)
    if(size(uwb(i).data,2)==4)
        R(1,i) = uwb(i).data(1).dist;
        R(2,i) = uwb(i).data(2).dist;
        R(3,i) = uwb(i).data(3).dist;
        R(4,i) = uwb(i).data(4).dist;
    end
end
%%
addpath('D:\Projects\MATLAB_scripts\TOF WLS')
[x] = TOF_WLS_2d(R,SatPos);
%%
for i=1:size(uwb,2)
    x0(:,i) = x(:,10,i);
end
%%
mx = mean(pos(1,:));
my = mean(pos(2,:));
%%
for i=1:size(wifi_meas,2)  
    RSSI = extractfield(wifi_meas(i).meas,'RSSI');
    [~,ind] = max(RSSI);
    wifi_meas_1AP(i).tag =  wifi_meas(i).tag;
    wifi_meas_1AP(i).timestamp =  wifi_meas(i).timestamp;
    wifi_meas_1AP(i).mass =  wifi_meas(i).mass;
    wifi_meas_1AP(i).meas = wifi_meas(i).meas(ind);  
end
%%
% i=1;
        try
            X_WkNN_grid_1m_1AP(:,i,k) = WkNN_method(wifi_meas_1AP(i),wifi_database_2m,k);          
        catch
            X_WkNN_grid_1m_1AP(:,i,k) = [NaN;NaN];
        end


%% kNN _algorithm_solver
[X_kNN_5m,X_kNN_1AP_5m] = kNN_algorithm_solver(wifi_meas,wifi_meas_1AP,wifi_database_5m);
%% WkNN _algorithm_solver
[X_WkNN_5m,X_WkNN_1AP_5m] = WkNN_algorithm_solver(...
    wifi_meas,wifi_meas_1AP,wifi_database_5m);
%% kNN_MLC_algorithm_solver
[X_kNN_MLC_5m,X_kNN_MLC_5m_1AP] = kNN_MLC_algorithm_solver(...
    wifi_meas,wifi_meas_1AP,wifi_database_5m);
%% kNN error
% [kNN_eps] = kNN_error(X_kNN_5m,mx,my);
[kNN_eps] = kNN_error(X_kNN_1AP_5m,mx,my);
%% WkNN error
% [WkNN_eps] = kNN_error(X_WkNN_5m,mx,my);
 [WkNN_eps] = kNN_error(X_WkNN_1AP_5m,mx,my);
%% kNN_MLC error
% [kNN_MLC_eps] = kNN_error(X_kNN_MLC_5m,mx,my);
[kNN_MLC_eps] = kNN_error(X_kNN_MLC_5m_1AP,mx,my);
%% ECDF plots
% plot_ECDF_err_all(X_WkNN_1m,mx,my)
plot_ECDF_err_all(X_kNN_MLC_1m,mx,my)
%% for dynamic error
% time_int = time(1):1e3:time(end);
% pos_int(1,:) = interp1(time,pos(1,:),meas_time(5:end-1));
% pos_int(2,:) = interp1(time,pos(2,:),me as_time(5:end-1));
% for k=1:11
%     for i=1:19
%         s_x(i,k) = (X_WkNN_grid_2m_1AP(1,i+4,k) - pos_int(1,i)).^2;
%         s_y(i,k) = (X_WkNN_grid_2m_1AP(2,i+4,k) - pos_int(2,i)).^2;
%     end
%     D_mx(k) = 1/(size(X_WkNN_grid_2m_1AP,2)-1) * sum(s_x(:,k));
%     D_my(k) = 1/(size(X_WkNN_grid_2m_1AP,2)-1)* sum(s_y(:,k));
%     eps(k) = sqrt(D_mx(k) + D_my(k));
% end
% disp([1:11;eps]);
%%
% meas_time = extractfield(wifi_meas,'timestamp');
% for i=1:length(meas_time)
%     for j=1:length(time)
%         if(abs((meas_time(i)-time(j)))*1e-9<0.15)
%             time_sync(i)=time(j);
%         end
%     end        
% end
% for k=1:11
%     for i=1:length(meas_time)
%         err_dyn_x(i,k) = X_WkNN_grid_2m_1AP2(1,i,k);
%     end
% end
%% CDF for WkNN
for k=1:11  
err_x_WkNN(k,:) = mx - X_WkNN_grid_2m_1AP(1,:,k);
% % A = histogram(err_x);
err_y_WkNN(k,:) = my - X_WkNN_grid_2m_1AP(2,:,k);
end
figure()
hold on
for k=1:4
    ecdf(sqrt(err_x_WkNN(k,:).^2+err_y_WkNN(k,:).^2))
end
% ecdf
legend('k=0','k=1','k=2','k=3','k=4','k=5','k=6','k=7','k=8','k=9','k=10')
%% CDF for kNN 
for k=1:11  
err_x_kNN(k,:) = mx - X_kNN_grid_2m_1AP(1,:,k);
% % A = histogram(err_x);
err_y_kNN(k,:) = my - X_kNN_grid_2m_1AP(2,:,k);
end

figure()
hold on
for k=1:11
    ecdf(sqrt(err_x_kNN(k,:).^2+err_y_kNN(k,:).^2))
end
legend('k=1','k=2','k=3','k=4','k=5','k=6','k=7','k=8','k=9','k=10')
%% CDF for MLC
% err_x_MLC = mx - X_MLC_grid_1m(1,:);
% % % A = histogram(err_x);
% err_y_MLC = my - X_MLC_grid_1m(2,:);
% figure()
% hold on
% % ecdf(sqrt(err_x_MLC.^2+err_y_MLC.^2))

%% CDF for kNN_MLC 
for k=1:11  
err_x_kNN_MLC(k,:) = (X_kNN_MLC_grid_6m(1,:,k)-mx).^2;
% % A = histogram(err_x);
err_y_kNN_MLC(k,:) = (X_kNN_MLC_grid_6m(2,:,k)-my).^2;
end
figure()
hold on
for k=1:10
    ecdf(sqrt(err_x_kNN_MLC(k,:).^2+err_y_kNN_MLC(k,:).^2))
end
legend('k=1','k=2','k=3','k=4','k=5','k=6','k=7','k=8','k=9','k=10')

%% plot MLC coord
figure()
plot(mean(x0(1,500:1500)),mean(x0(2,500:1500)),'x')
hold on
plot(X_MLC_grid_1m(1,:),X_MLC_grid_1m(2,:),'*')
xlabel('X,м')
grid on
ylabel('Y ,м')
title('Оценка коориднат в плане')
%% plot MLC err
figure()
plot(mean(x0(1,500:1500))-X_MLC_grid_1m(1,:))
xlabel('Номер измерения')
grid on
ylabel('Величина ошибки,м')
title('Зависимость ошибки по X от времени')
figure()
plot(mean(x0(2,500:1500))-X_MLC_grid_1m(2,:))
xlabel('Номер измерения')
grid on
ylabel('Величина ошибки,м')
title('Зависимость ошибки по Y от времени')
%% Plot WkNN coord
figure()
plot(mean(x0(1,500:1500)),mean(x0(2,500:1500)),'x')
hold on
plot(X_WkNN_grid_2m_1AP2(1,:,1),X_WkNN_grid_2m_1AP2(2,:,1),'*')
hold on
plot(X_WkNN_grid_2m_1AP2(1,:,2),X_WkNN_grid_2m_1AP2(2,:,2),'*')
plot(X_WkNN_grid_2m_1AP2(1,:,3),X_WkNN_grid_2m_1AP2(2,:,3),'*')
plot(X_WkNN_grid_2m_1AP2(1,:,4),X_WkNN_grid_2m_1AP2(2,:,4),'*')
plot(X_WkNN_grid_2m_1AP2(1,:,5),X_WkNN_grid_2m_1AP2(2,:,5),'*')
xlabel('X,м')
grid on
ylabel('Y,м')
title('Оценка коориднат в плане')
%% Plot WkNN err
figure()
plot(mean(x0(1,500:1500))-X_WkNN_grid_2m_1AP2(1,:,1),'-x')
% hold on
% plot(mean(x0(1,500:1500))-X_WkNN_grid_1m(1,:,2),'-x')
% plot(mean(x0(1,500:1500))-X_WkNN_grid_1m(1,:,3),'-x')
% plot(mean(x0(1,500:1500))-X_WkNN_grid_1m(1,:,4),'-x')
% plot(mean(x0(1,500:1500))-X_WkNN_grid_1m(1,:,6),'-x')
xlabel('Номер измерения')
grid on
ylabel('Величина ошибки,м')
title('Зависимость ошибки по X от времени')
figure()
%%
figure()
plot(mean(x0(2,500:1500))-X_WkNN_grid_2m_1AP2(2,:,1))
xlabel('Номер измерения')
grid on
ylabel('Величина ошибки,м')
title('Зависимость ошибки по Y от времени')
%%
figure()
hold on
for k=1:10
plot( X_WkNN_grid_8m(2,:,k));
end
plot(X_MLC_grid_8m(2,:),'*')
%%
for i = 1:167
    TF = contains(extractfield(wifi_meas(i).meas,'SSID'),'ESPap6');
    if(TF(TF==1))
        RSSI_5(i) = wifi_meas(i).meas(TF).RSSI;
    else
        RSSI_5(i) = NaN;
    end
end
%%
figure
plot((timestamp(:)-timestamp(1))*1e-9,RSSI)
hold on
plot((timestamp(:)-timestamp(1))*1e-9,RSSI_2)
plot((timestamp(:)-timestamp(1))*1e-9,RSSI_3)
plot((timestamp(:)-timestamp(1))*1e-9,RSSI_4)
plot((timestamp(:)-timestamp(1))*1e-9,RSSI_5)
grid on
xlabel('время, с')
ylabel('RSSI, дБмВт')