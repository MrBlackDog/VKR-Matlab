function [wifi_smart_meas] = wifi_smart_log_reader()
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    if nargin == 0
        [file, path] = uigetfile('*.*');
        filename = fullfile(path,file);  
    end
    f = fopen(filename);
    for i=1:6
         current_line = fgetl(f);
         header = strsplit(current_line);
    end
    j=0;
    while feof(f) ~= 1
        j=j+1;
        current_line = fgetl(f); 
        wifi_mass = split(current_line,' ');
        wifi_smart_meas(j).tag = convertCharsToStrings(wifi_mass{1});
        wifi_smart_meas(j).timestamp = str2num(wifi_mass{2});
        wifi_smart_meas(j).mass = wifi_mass(3:end);
    end
    
    for i=1:size(extractfield(wifi_smart_meas,'mass'),2)
            wifi_smart_meas(i).meas.SSID = [];
            wifi_smart_meas(i).meas.BSSID = [];
            wifi_smart_meas(i).meas.RSSI = [];
    end
    
    for i=1:size(extractfield(wifi_smart_meas,'mass'),2)% proxodimsya po vsem pachkam
        for j=1:3:size(wifi_smart_meas(i).mass,1)%proxodimsya po vsem izmereniyam pachkam
            disp([i j])
%             splited_meas_mass = strsplit(wifi_smart_meas(i).mass{j},' ');
%             for k=1:size(splited_meas_mass,2)
%                 splited_meas = strsplit(splited_meas_mass{k},'+');
                if(~strcmp(wifi_smart_meas(i).mass(j),''))
                    if(isempty(wifi_smart_meas(i).meas(1).BSSID))
                        wifi_smart_meas(i).meas((j+2)/3).SSID = wifi_smart_meas(i).mass{j};
                        wifi_smart_meas(i).meas((j+2)/3).BSSID = wifi_smart_meas(i).mass{j+1};
                        wifi_smart_meas(i).meas((j+2)/3).RSSI = str2double(wifi_smart_meas(i).mass{j+2});
                        count = 1;
                    else
                        TF = contains(extractfield(wifi_smart_meas(i).meas,'BSSID'),wifi_smart_meas(i).mass{j+1});
                            %                     elseif(contains(extractfield(wifi_meas(i).meas,'BSSID'),splited_meas{2}))
                            if(~isempty(find(TF==1, 1)))
                                wifi_smart_meas(i).meas(TF).RSSI = [wifi_smart_meas(i).meas(TF).RSSI str2double(wifi_smart_meas(i).mass{j+2})];
                            else
                                count = count+1;
                                wifi_smart_meas(i).meas(count).SSID = wifi_smart_meas(i).mass{j};
                                wifi_smart_meas(i).meas(count).BSSID = wifi_smart_meas(i).mass{j+1};
                                wifi_smart_meas(i).meas(count).RSSI = str2double(wifi_smart_meas(i).mass{j+2});
                            end
                    end
%                 end
                end
        end
    end
end

