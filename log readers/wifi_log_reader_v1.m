function [wifi_meas] = wifi_log_reader_v1()
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
if nargin == 0
    [file, path] = uigetfile('*.*');
    filename = fullfile(path,file);  
end
f = fopen(filename);
pointscount = 0;
indexstring = {};
while 1
  s=fgetl(f);
  if contains(s,'measurements started')
    break;
  else
    pointscount = pointscount + 1;
    coordinate_mass = split(s,':');
    indexstring = [indexstring, s];
    wifi_meas(pointscount).coordinate.x = (coordinate_mass{1});
    wifi_meas(pointscount).coordinate.y = (coordinate_mass{2});
  end
end
for i=1:pointscount
    wifi_meas(pointscount).raw_meas = [];
end
j=0;
while feof(f)==0 
        s=fgetl(f);
        splited_string = split(s,' ');
        j=j+1;
        disp(j);
%         coordinate_mass = split(splited_string(1),':');
%         TF = strmatch(indexstring,convertCharsToStrings(splited_string{1}));
        TF = matches(indexstring,convertCharsToStrings(splited_string{1}));
        wifi_meas(TF).raw_meas =[wifi_meas(TF).raw_meas; splited_string(2)];
end