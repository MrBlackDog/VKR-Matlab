function [uwb_meas] = uwb_log_reader()
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
if nargin == 0
    [file, path] = uigetfile('*.*');
    filename = fullfile(path,file);  
end
f = fopen(filename);
while 1
  s=fgetl(f);
  if s(1:end) == "# UWB,ElapsedRealtimeNanos,data"
    break;
  end
end
meas_count = 0;
while feof(f)==0
    s=fgetl(f);
    splited_string = split(s,' ');
%     TF = contains(splited_string,"UWB")
    if(contains(s,"UWB") && ~ contains(s,"les"))
        meas_count=meas_count+1;
        uwb_meas(meas_count).timestamp = splited_string(2);
        for i=1:size(splited_string,1)-2
%         uwb_meas(meas_count).data(i) = splited_string(i+2);

            cur_meas_data = splited_string{i+2};
            if (~isempty(cur_meas_data))
                if(contains(cur_meas_data,"est"))
                    est_mass = strsplit(cur_meas_data(5:21),',');
                    tag_pos(1) = str2num(est_mass{1,1});
                    tag_pos(2) = str2num(est_mass{1,2});
                    tag_pos(3) = str2num(est_mass{1,3});
                    uwb_meas(meas_count).tag_pos = tag_pos;
                elseif(contains(cur_meas_data,"le_us"))
                else
                    id = cur_meas_data(1:4);
                    pos_mass = strsplit(cur_meas_data(6:19),',');
                    pos(1) = str2num(pos_mass{1,1});
                    pos(2) = str2num(pos_mass{1,2});
                    pos(3) = str2num(pos_mass{1,3});
                    range = str2num(cur_meas_data(22:25));
                    uwb_meas(meas_count).data(i).id = id;
                    uwb_meas(meas_count).data(i).anch_pos = pos;
                    uwb_meas(meas_count).data(i).dist = range;
                end
            end
        end
    end
end
fclose(f);
end