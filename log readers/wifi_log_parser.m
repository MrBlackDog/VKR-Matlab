function [wifi_database2] = wifi_log_parser(wifi_database)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
%     wifi_meas.meas{};
%     wifi_meas.meas(1).RSSI = {};
%     p = parpool(4);
    for i=1:size(wifi_database,2)
            wifi_database(i).meas.SSID = [];
            wifi_database(i).meas.BSSID = [];
            wifi_database(i).meas.RSSI = [];
            wifi_database(i).meas.meanRSSI = [];
            wifi_database(i).meas.sigmaRSSI = [];
    end

    parfor i=1:size(wifi_database,2)
        for j=1:size(wifi_database(i).raw_meas,1)
            splited_meas_mass = strsplit(wifi_database(i).raw_meas{j},'|');
            for k=1:size(splited_meas_mass,2)
                disp([i j k]);
                splited_meas = strsplit(splited_meas_mass{k},'+');
                if(~strcmp(splited_meas{1},''))
                    %���������� ���������
                    if(isempty(wifi_database(i).meas(1).BSSID))
                        wifi_database(i).meas(k).SSID = splited_meas{1};
                        wifi_database(i).meas(k).BSSID = splited_meas{2};
                        wifi_database(i).meas(k).RSSI = str2double(splited_meas{3});
                        count = 1;
%                     if(isempty(wifi_meas.meas(k).SSID))
%                          wifi_meas.meas(k).SSID = [];
                    %������ ����������
                    else
                        TF = contains(extractfield(wifi_database(i).meas,'BSSID'),splited_meas{2});
                            %                     elseif(contains(extractfield(wifi_meas(i).meas,'BSSID'),splited_meas{2}))
                            if(~isempty(find(TF==1, 1)))
                                wifi_database(i).meas(TF).RSSI = [wifi_database(i).meas(TF).RSSI str2double(splited_meas{3})];
                            else
                                count = count+1;
                                wifi_database(i).meas(count).SSID = splited_meas{1};
                                wifi_database(i).meas(count).BSSID = splited_meas{2};
                                wifi_database(i).meas(count).RSSI =  str2double(splited_meas{3});
                            end
%                         wifi_meas(i).meas.SSID = [wifi_meas(i).meas.SSID, splited_meas{1}];
%                         wifi_meas(i).meas.BSSID = [wifi_meas(i).meas.BSSID, splited_meas{2}];
%                         wifi_meas(i).meas.RSSI = [wifi_meas(i).meas.RSSI, splited_meas{3}];
                    end
                        
                    %�������� � ����� ����
%                     wifi_meas(i).meas(j,k).SSID = splited_meas{1};
%                     wifi_meas(i).meas(j,k).BSSID = splited_meas{2};
%                     wifi_meas(i).meas(j,k).RSSI = splited_meas{3};
                    
                   
                    
%                     if(contains(splited_meas{1}, wifi_meas(i).meas(k).SSID))
%                         TF = contains(str,pat)
                   
                end
            end
        end
    end
    %% mean and std calculation
    for i=1:size(wifi_database,2)
        for j=1:size(wifi_database(i).meas,2)
            if(size(wifi_database(i).meas(j).RSSI,2)>100)
                wifi_database(i).meas(j).meanRSSI = mean(wifi_database(i).meas(j).RSSI);
                wifi_database(i).meas(j).sigmaRSSI = std(wifi_database(i).meas(j).RSSI);
                %                 else
                %                     wifi_meas(i).meas(j) = [];
            end
        end
    end
    %% error database removal
[wifi_database2] = error_database_removal(wifi_database);
%     for i=1:size(wifi_meas,2)
%         for j=1:size(wifi_meas(i).meas,2)
%                 if(size(wifi_meas(i).meas(1).RSSI,2)>150)
%                    wifi_meas(i).meas(j).meanRSSI = mean(wifi_meas(i).meas(j).RSSI);
%                 else
%                     wifi_meas(i).meas(j) = [];
%                 end
%         end
%     end
end