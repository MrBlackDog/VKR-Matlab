function [eps] = WkNN_error(X_WkNN,mx,my)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
for k=1:11
    for i=1:size(X_WkNN,2)
        s_x(i,k) = (X_WkNN(1,i,k)-mx)^2;
        s_y(i,k) = (X_WkNN(2,i,k)-my)^2;
    end
    D_mx(k) = 1/(size(X_WkNN,2)-1) * sum(s_x(:,k));
    D_my(k) = 1/(size(X_WkNN,2)-1)* sum(s_y(:,k));
    eps(k) = sqrt(D_mx(k) + D_my(k));  
end
 disp([1:11;eps]);
end

