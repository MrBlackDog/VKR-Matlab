function [X] = Wifi_Nav_Solver(wifi_meas,wifi_database)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
X = zeros(2,size(wifi_meas,2));
    for k=1:size(wifi_meas,2)
        X(:,k) = Solver(wifi_meas(k),wifi_database);
    end
end
function [X] = Solver(current_wifi_meas,wifi_database) 
    X = [0;0];
    number_of_matches = zeros(size(wifi_database,2),1);
    for i=1:size(wifi_database,2)
%         for j=1:size(wifi_database(i).meas,2)
            for m=1:size(current_wifi_meas.meas,2)
                TF = matches(extractfield(wifi_database(i).meas,'BSSID'),current_wifi_meas.meas(m).BSSID,'IgnoreCase',1);
                if(TF(TF==1))
                    diff(m,i) = current_wifi_meas.meas(m).RSSI - wifi_database(i).meas(TF).meanRSSI;
%                     if(abs(diff(m,i))<3)
                    if(abs(diff(m,i))<wifi_database(i).meas(TF).sigmaRSSI)
                        number_of_matches(i) = number_of_matches(i)+1;
                    end
                end
            end
%         end
    end
    [val, ind] = max(number_of_matches);
    X(1,1) =  str2double(wifi_database(ind).coordinate.x);
    X(2,1) =  str2double(wifi_database(ind).coordinate.y);
end
