function [wifi_meas2] = error_meas_removal(wifi_meas)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
k=1;
wifi_meas2(1) = wifi_meas(1);
for i=2:size(wifi_meas,2)
    str1 = extractfield(wifi_meas(i-1).meas,'SSID');
    str2 = extractfield(wifi_meas(i).meas,'SSID');
    if(size(str1,2) == size(str2,2))
        TF =strcmpi(str1,str2);
        RSSI1 = extractfield(wifi_meas(i-1).meas,'RSSI');
        RSSI2 = extractfield(wifi_meas(i).meas,'RSSI');
        TF2 = RSSI1 == RSSI2;
        if(~(isempty(TF(TF~=1)) && isempty(TF2(TF2~=1))))
            k = k+1;
            wifi_meas2(k) = wifi_meas(i);
        end
    else
        k = k+1;
        wifi_meas2(k) = wifi_meas(i);
    end
    str3 = extractfield(wifi_meas2(k).meas,'SSID');
    TFS =  matches(str3,'DESKTOP');
    wifi_meas2(k).meas(TFS) = [];
end
end

