function [P] = MLC_algorithm(s,wifi_database)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
for i=1:size(wifi_database,2)
    %     for i=1:size(wifi_database(j).meas,2)
    for m=1:size(s.meas,2)
%     disp([i,m])
        TF = matches(extractfield(wifi_database(i).meas,'BSSID'),s.meas(m).BSSID,'IgnoreCase',1);
        if(TF(TF==1))
            P(i,m) = 1/(wifi_database(i).meas(TF).sigmaRSSI*sqrt(2*pi))...
                * exp(-1*0.5*( (s.meas(m).RSSI-wifi_database(i).meas(TF).meanRSSI)/wifi_database(i).meas(TF).sigmaRSSI)^2);
        else
            P(i,m) = 0;
        end
    end
end
end