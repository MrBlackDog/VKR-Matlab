function [X_kNN_MLC,X_kNN_MLC_1AP] = kNN_MLC_algorithm_solver(wifi_meas,wifi_meas_1AP,wifi_database)

parfor i=1:size(wifi_meas,2)   
    for  k=1:11
        try
            X_kNN_MLC(:,i,k) = kNN_MLC_algorithm(wifi_meas(i),wifi_database,k);
        catch
            X_kNN_MLC(:,i,k) = [NaN;NaN];
        end
        try
            X_kNN_MLC_1AP(:,i,k) = kNN_MLC_algorithm(wifi_meas_1AP(i),wifi_database,k);
        catch
            X_kNN_MLC_1AP(:,i,k) = [NaN;NaN];
        end
    end
end
end

