function [eps] = kNN_error(X_kNN,mx,my)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

for k=1:11
    count = 0;
    for i=1:size(X_kNN,2)
        if ~isnan(X_kNN(1,i,k))
            count = count + 1;
            s_x(count,k) = (X_kNN(1,i,k)-mx)^2;
            s_y(count,k) = (X_kNN(2,i,k)-my)^2;
        end
    end
    if(count~=0)
        D_mx(k) = 1/(count-1) * sum(s_x(1:count,k));
        D_my(k) = 1/(count-1) * sum(s_y(1:count,k));
        eps(k) = sqrt(D_mx(k) + D_my(k));
    else
        eps(k) = NaN;
    end
end
disp([1:11;eps]);
end

