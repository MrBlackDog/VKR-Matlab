function [X_WkNN,X_WkNN_1AP] = WkNN_algorithm_solver(wifi_meas,wifi_meas_1AP,wifi_database)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
parfor i=1:size(wifi_meas,2)    
    for k=1:11
        %k=1 is the same point
        try
            X_WkNN(:,i,k) = WkNN_method(wifi_meas(i),wifi_database,k);
        catch
            X_WkNN(:,i,k) = [NaN;NaN];
        end
        try
            X_WkNN_1AP(:,i,k) = WkNN_method(wifi_meas_1AP(i),wifi_database,k);
        catch
            X_WkNN_1AP(:,i,k) = [NaN;NaN];
        end
    end
end
end